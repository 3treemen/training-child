<?php
/**
 * Template Name: Login Page 
 */
 
 get_header(); 
 
 
 if (is_user_logged_in()){
	 
	     echo '<div class="container"><div class="welkom">Welkom. Je bent ingelogd!</div></div>'; 
		 
 }else{ 
	 	echo '<div id="loginform-wrapper">';
	 	echo '<div class="container loginheader"><h1>Inloggen</h1></div>';
		wp_login_form( 

			array(
					'echo'           => true,
					// Default 'redirect' value takes the user back to the request URI.
					'redirect'       => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
					'form_id'        => 'loginform',
					'label_username' => __( 'Gebruikersnaam of e-mailadres' ),
					'label_password' => __( 'Wachtwoord' ),
					'label_remember' => __( 'Onhoud mij' ),
					'label_log_in'   => __( 'Inloggen' ),
					'id_username'    => 'user_login',
					'id_password'    => 'user_pass',
					'id_remember'    => 'rememberme',
					'id_submit'      => 'wp-submit',
					'remember'       => true,
					'value_username' => '',
					// Set 'value_remember' to true to default the "Remember me" checkbox to checked.
					'value_remember' => false,
				)
							
			);
			echo '<div class="center lostpw"><a href="'. wp_lostpassword_url( home_url() ) . '">Wachtwoord vergeten?</a></div>';
		echo '</div>';
    }			
	
get_footer();		
				