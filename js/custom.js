
(function($) {	
    "use strict";
	
    $(document).ready(function() {	        
        jQuery('#learndash-lesson-display-content-settings_auto_approve_assignment-on').prop('checked', true);
        jQuery('#learndash-topic-display-content-settings_auto_approve_assignment-on').prop('checked', true);
    });
})( jQuery );