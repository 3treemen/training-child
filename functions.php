<?php
 
 function overwrite_language_file_child_theme() {
    $lang = get_stylesheet_directory().'/lang';
    return $lang;
}
add_filter('ava_theme_textdomain_path', 'overwrite_language_file_child_theme');

/* style login page, it's outside the theme */
function my_login_page_styles() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/logo-small.png);
            height:150px;
            width:250px;
            background-size: 250px 150px;
            background-repeat: no-repeat;
        	padding-bottom: 30px;
        }
        body.login #nav, 
        body.login .language-switcher {
            display: none;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_page_styles' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

/* redirect all anonymous visitors to the login page */

function password_protected() {
    if ( !is_user_logged_in() )
        auth_redirect();
}

add_action('login_head', 'rsd_link');
add_action('login_head', 'wlwmanifest_link');
add_action('template_redirect', 'password_protected');
add_action('do_feed', 'password_protected');

/* change the default from name */
function change_my_sender_name( $original_email_from ) {
    return 'Alterme';
}
add_filter( 'wp_mail_from_name', 'change_my_sender_name' );


// default js does not support fullscreen video
// so we need to fix this in the avia.js file
function wp_change_aviajs() {
	wp_dequeue_script( 'avia-default' );
	wp_enqueue_script( 'avia-default-child', get_stylesheet_directory_uri().'/avia.js', array('jquery'), 2, true );
    wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri() . '/js/custom.js', array( 'jquery' ), 3, true );

	}
	add_action( 'wp_print_scripts', 'wp_change_aviajs', 100 );

    function avf_alb_supported_post_types_mod( array $supported_post_types )
    {
      $supported_post_types[] = 'sfwd-courses';
      $supported_post_types[] = 'sfwd-lessons';
      $supported_post_types[] = 'sfwd-topic';
      $supported_post_types[] = 'sfwd-quiz';
      return $supported_post_types;
    }
// add_filter('avf_alb_supported_post_types', 'avf_alb_supported_post_types_mod', 10, 1);
    
function avf_metabox_layout_post_types_mod( array $supported_post_types )
{
    $supported_post_types[] = 'sfwd-courses';
    $supported_post_types[] = 'sfwd-lessons';
    $supported_post_types[] = 'sfwd-topic';
    $supported_post_types[] = 'sfwd-quiz';
    return $supported_post_types;
}
// add_filter('avf_metabox_layout_post_types', 'avf_metabox_layout_post_types_mod', 10, 1);

add_theme_support( 'deactivate_layerslider' );


